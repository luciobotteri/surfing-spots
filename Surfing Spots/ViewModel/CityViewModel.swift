//
//  CityViewModel.swift
//  Surfing Spots
//
//  Created by Lucio Botteri on 13/04/21.
//

import Combine

class CityViewModel: ObservableObject {
    
    @Published var cities = [City]()
    
    var cancellationTokens = Set<AnyCancellable>()
    
    init() {
        getCities()
    }
    
    /// Downloads the list of the cities and then calls the [getTemperature](x-source-tag://getTemperature) method.
    func getCities() {
        NetworkLayer.getCities()
            .mapError { (error) -> Error in
                print("Error: \(error.localizedDescription)")
                return error
            }
            .map(\.cities)
            .map({ cities in
                return cities.map { City(name: $0.name) }
            })
            .sink { _ in }
                receiveValue: {
                    [weak self] in
                    self?.cities = $0
                    self?.getTemperatures()
                }
            .store(in: &cancellationTokens)
    }
    
    /// Downloads temperature for a single random city in the list and sorts the array accordingly
    /// - Tag: getTemperature
    func getTemperature() {
        NetworkLayer.getTemperature()
            .mapError { (error) -> Error in
                print("Error: \(error.localizedDescription)")
                return error
            }
            .sink { _ in }
                receiveValue: {
                    [weak self] in
                    self?.cities.randomElement()?.temperature = $0
                    self?.cities.sort(by: { $0.temperature ?? 0 > $1.temperature ?? 0 })
                    self?.objectWillChange.send()
                }
            .store(in: &cancellationTokens)
    }
    
    /// Downloads temperature for every city in the list and sorts the array accordingly
    func getTemperatures() {
        cities.map { city in
            return NetworkLayer.getTemperature(for: city)
                .map { temperature in
                    city.temperature = temperature
                    return city
                }
                .eraseToAnyPublisher()
                .replaceError(with: city)
        }
        .publisher
        .flatMap { $0 }
        .collect()
        .sink { _ in }
            receiveValue: {
                [weak self] in
                self?.cities = $0.sorted(by: { $0.temperature ?? 0 > $1.temperature ?? 0 })
                self?.objectWillChange.send()
            }
        .store(in: &cancellationTokens)
    }
}
