//
//  Surfing_SpotsApp.swift
//  Surfing Spots
//
//  Created by Lucio Botteri on 05/04/21.
//

import SwiftUI

@main
struct Surfing_SpotsApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
