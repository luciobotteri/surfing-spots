//
//  UIColor+random.swift
//  Surfing Spots
//
//  Created by Lucio Botteri on 25/04/21.
//

import SwiftUI

extension Color {
    /// Returns a random color
    static var random: Color {
        return Color(hue: .random(in: 0...1), saturation: 1, brightness: 0.7, opacity: 1)
    }
}
