//
//  City.swift
//  Surfing Spots
//
//  Created by Lucio Botteri on 18/04/21.
//

import Foundation

/// - Tag: CityResponse
struct CityResponse: Codable {
    let name: String
}

struct CitiesResponse: Codable {
    let cities: [CityResponse]
}

class City: Identifiable, ObservableObject {
    let id = UUID()
    let name: String
    @Published var temperature: Int?
    /// Returns a string describing the weather of the city
    var weather: String {
        if let t = temperature {
            return t < 30 ? "Cloudy" : "Sunny"
        }
        return ""
    }
    
    init(name: String) {
        self.name = name
    }
    
    func temperature(_ temperature: Int) -> City {
        self.temperature = temperature
        return self
    }
}
