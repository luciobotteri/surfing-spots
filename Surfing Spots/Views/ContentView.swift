//
//  ContentView.swift
//  Surfing Spots
//
//  Created by Lucio Botteri on 05/04/21.
//

import SwiftUI

struct ContentView: View {
    
    @ObservedObject private var cityViewModel = CityViewModel()
    
    let timer = Timer.publish(every: 3, on: .main, in: .common)
        .autoconnect()
    
    var body: some View {
        if cityViewModel.cities.isEmpty {
            ProgressView()
        } else {
            NavigationView {
                ScrollView {
                    ForEach(cityViewModel.cities) {
                        CityCellView(city: $0)
                            .padding(.bottom, 8)
                    }.animation(.easeInOut)
                }.navigationTitle("Surfing Spots")
            }
            .onReceive(timer, perform: { _ in
                DispatchQueue.main.asyncAfter(deadline: .now() + 3) {
                    cityViewModel.getTemperature()
                }
            })
        }
    }
}
