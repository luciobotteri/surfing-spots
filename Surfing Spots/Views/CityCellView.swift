//
//  CityCellView.swift
//  Surfing Spots
//
//  Created by Lucio Botteri on 25/04/21.
//

import SwiftUI

struct CityCellView: View {
    
    @ObservedObject var city: City
    
    var body: some View {
        ZStack {
            if city.temperature ?? 0 < 30 {
                Rectangle()
                    .foregroundColor(Color(white: 0.25))
            } else {
                Image(city.name)
                    .resizable()
                    .aspectRatio(contentMode: .fill)
                Rectangle()
                    .fill(
                        LinearGradient(gradient: Gradient(colors: [.clear, Color(white: 0, opacity: 0.3)]), startPoint: .top, endPoint: .bottom)
                    )
            }
            VStack(alignment: .leading) {
                Spacer()
                HStack {
                    Text(city.name)
                        .font(.title)
                        .foregroundColor(.white)
                        .padding(.leading, 10)
                        .shadow(radius: 4)
                    Spacer()
                }.padding(.top, 60)
                HStack {
                    if let temperature = city.temperature {
                        Text(city.weather + " - \(temperature) degrees")
                            .font(.subheadline)
                            .foregroundColor(.white)
                            .fontWeight(.medium)
                            .padding(.leading, 10)
                            .shadow(radius: 4)
                    } else {
                        ProgressView()
                            .foregroundColor(.white)
                            .padding(.leading, 30)
                    }
                    Spacer()
                }
                Spacer()
            }
        }
        .frame(height: 160)
        .cornerRadius(8)
        .padding(.horizontal, 20)
    }
}

struct CityCellView_Previews: PreviewProvider {
    static var previews: some View {
        CityCellView(city:
                        City(name: "Cuba")
                        .temperature(30)
        )
    }
}
