//
//  NetworkLayer.swift
//  Surfing Spots
//
//  Created by Lucio Botteri on 10/04/21.
//

import Foundation
import Combine

class NetworkLayer {
    static private let apiClient = APIClient()
    static private let getCityUrl = URL(string: "https://run.mocky.io/v3/652ceb94-b24e-432b-b6c5-8a54bc1226b6")!
    static private let getNumberUrl = URL(string: "http://numbersapi.com/random/math")!
    
    /// Downloads the list of the cities
    /// - Returns: A publisher with a [CityResponse](x-source-tag://CityResponse) or an Error
    static func getCities() -> AnyPublisher<CitiesResponse, Error> {
        guard let components = URLComponents(url: getCityUrl, resolvingAgainstBaseURL: true)
        else { fatalError("Couldn't create URLComponents") }
        
        let request = URLRequest(url: components.url!)
        
        return apiClient.run(request)
            .map(\.value)
            .eraseToAnyPublisher()
    }
    
    /// Calls the service to get the temperature of a given city
    /// - Parameter city: The city whose temperature we want to get
    /// - Returns: A publisher with the temperature of the city, or an error
    static func getTemperature(for city: City? = nil) -> AnyPublisher<Int, Error> {
        // City parameter will be used with real data
        guard var components = URLComponents(url: getNumberUrl, resolvingAgainstBaseURL: true)
        else { fatalError("Couldn't create URLComponents") }
        components.queryItems = [
            URLQueryItem(name: "min", value: "22"),
            URLQueryItem(name: "max", value: "40")
        ]
        
        let request = URLRequest(url: components.url!)
        
        return apiClient.runRaw(request)
            .map(\.value)
            .map { (data: Data) in
                let string = String(data: data, encoding: .utf8)!
                return Int(string.prefix(2)) ?? -999
            }
            .eraseToAnyPublisher()
    }
}
